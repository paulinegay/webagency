const items = document.getElementsByClassName('mySlides');
const nbSlide = items.length;
const next = document.getElementsByClassName('btn-right')[0];
const prev = document.getElementsByClassName('btn-left')[0];
let count = 0;

function nextSlide() {
    items[count].classList.remove('active')

    if (count < nbSlide - 1) {
        count++;
    }
    else {
        count = 0;
    }

    items[count].classList.add('active');

}


function prevSlide() {
    items[count].classList.remove('active')

    if (count > 0) {
        count--;
    }
    else {
        count = nbSlide - 1;
    }

    items[count].classList.add('active')
}

prev.addEventListener('click', prevSlide);

next.addEventListener('click', nextSlide);